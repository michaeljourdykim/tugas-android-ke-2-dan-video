package com.stts.wawan.tugas1

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.home_activity.*

class SecondActivity : AppCompatActivity() {

    companion object {
        fun getStartIntent(
                context: Context,
                us: String
        ) = Intent(context, SecondActivity::class.java).apply {
            putExtra("user", us)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        val user_login = intent.getStringExtra("user")
        hasil.text = "Welcome, $user_login"
    }

}
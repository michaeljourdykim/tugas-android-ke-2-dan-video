package com.stts.wawan.tugas1

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.display_activity.*

class RecentFragment : Fragment() {

    companion object {
        fun newInstance() = RecentFragment()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                R.layout.display_activity,
                container,
                false
        )
    }

    fun cekLogin(tulisan: String) {
        text_status.append("$tulisan\n")
    }


}